package net.daemonyum.manticora.framework.web.service.servlet;

import javax.servlet.http.HttpSession;

import net.daemonyum.manticora.framework.web.service.SendDataMethod;

/**
 * 
 * @author Kiyous
 *
 */
public abstract class PutServlet extends AbstractManticoraServlet {

	protected PutServlet() {
		super(SendDataMethod.PUT);
	}
	
	public abstract void doPut(HttpSession session);

	public abstract void destroy();
}
