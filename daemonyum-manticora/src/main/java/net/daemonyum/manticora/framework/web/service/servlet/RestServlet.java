package net.daemonyum.manticora.framework.web.service.servlet;

import net.daemonyum.manticora.framework.web.bean.ManticoraEntity;
import net.daemonyum.manticora.framework.web.service.SendDataMethod;
import net.daemonyum.manticora.framework.web.service.view.ServletResource;

/**
 * Une RestServlet est une Servlet Manticora 
 * 
 * @author Kiyous
 *
 */
public abstract class RestServlet extends AbstractManticoraServlet {

	protected RestServlet() {super(SendDataMethod.REST);}

	@Override
	public abstract void destroy();

	public abstract ServletResource doDelete(ManticoraEntity entity);

	public abstract ServletResource doGet();

	public abstract ServletResource doPost(ManticoraEntity entity);

	public abstract ServletResource doPut(ManticoraEntity entity);
}
