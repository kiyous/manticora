package net.daemonyum.manticora.framework.web.service.servlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

/**
 * 
 * @author Kiyous
 *
 */
@SuppressWarnings("serial")
public abstract class LoadOnStartupServlet extends HttpServlet {
	
	@Override
	public void init() throws ServletException {		
		//super.init();
		doInit(getServletContext());
	}
		
	protected abstract void doInit(ServletContext context);

}
