package net.daemonyum.manticora.framework.web.service.servlet;

import net.daemonyum.manticora.framework.web.bean.ManticoraBean;
import net.daemonyum.manticora.framework.web.service.SendDataMethod;
import net.daemonyum.manticora.framework.web.service.view.ServletRedirectUrl;

/**
 * 
 * @author Kiyous
 */
public abstract class PostServlet extends AbstractManticoraServlet {

	protected PostServlet() {
		super(SendDataMethod.POST);
	}

	@Override
	public abstract void destroy();

	public abstract ServletRedirectUrl doPost(ManticoraBean bean);
}
