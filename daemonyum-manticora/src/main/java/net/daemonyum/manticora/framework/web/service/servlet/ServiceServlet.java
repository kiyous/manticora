package net.daemonyum.manticora.framework.web.service.servlet;

import net.daemonyum.manticora.framework.web.bean.ManticoraBean;
import net.daemonyum.manticora.framework.web.service.SendDataMethod;
import net.daemonyum.manticora.framework.web.service.view.ServletRedirectUrl;

/**
 * 
 * @author Kiyous
 *
 */
public abstract class ServiceServlet extends AbstractManticoraServlet {

	protected ServiceServlet() {
		super(SendDataMethod.SERVICE);
	}

	@Override
	public abstract void destroy();

	public abstract ServletRedirectUrl doGetOrPost(ManticoraBean bean);
}
