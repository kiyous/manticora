package net.daemonyum.manticora.framework.web.service.servlet.manager;

import java.io.IOException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import net.daemonyum.manticora.framework.web.exception.NotFoundException;
import net.daemonyum.manticora.framework.web.service.servlet.ManticoraServlet;

/**
 * 
 * @author Kiyous
 *
 */
@SuppressWarnings("serial")
public abstract class DispatcherServlet extends HttpServlet {

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(DispatcherServlet.class);

	@Override
	public void init() throws ServletException {	
		super.init();
	}

	public abstract void monitoring(HttpServletRequest request, HttpServletResponse response);

	@Override
	protected final void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ServletContext context = getServletContext();
		//    	String url = request.getRequestURL().toString();    	
		//		String path = url.substring(url.lastIndexOf(context.getServletContextName()));		
		String path = request.getRequestURI().toString();

		try {
			monitoring(request, response);
		}catch(Exception e) {
			LOGGER.warn("Une erreur est survenue dans la méthode monitoring ("+path+")", e);
		}			

		// décomposer l'url
		// récupérer la servlet de l'enum
		// appeler la methode serve
		ManticoraServlet servlet = null;	
		try {	
			LOGGER.trace(path);

			Pattern pattern = Pattern.compile("(.*)\\.(.*)");
			Matcher matcher = pattern.matcher(path);


			if(path.matches("(.*)\\.(.*)")) {

				@SuppressWarnings("unchecked")
				Map<ContextAttributeType, ContextAttribute> contextMap = (Map<ContextAttributeType, ContextAttribute>) context.getAttribute(path);			
				if(contextMap == null) {
					// erreur 404
					throw new NotFoundException("");
				}else {				
					Class<? extends ManticoraServlet> myClass = contextMap.get(ContextAttributeType.SERVLET).getServlet();
					servlet = myClass.newInstance();
					ContextAttribute contextParams = contextMap.get(ContextAttributeType.PARAMS);
					if(contextParams != null) {	
						Map<String, String> params = contextParams.getParameters();
						for (String mapKey : params.keySet()) {
							request.setAttribute(mapKey, params.get(mapKey));
						}
					}
					servlet.serve(request, response);
					servlet.destroy();
				}
			}else {
				String contextName = context.getServletContextName().isEmpty()?"":"/"+context.getServletContextName();
				path = path.replace(contextName, "");
				throw new NotFoundException("");

			}

		}catch(InstantiationException e) {			
			LOGGER.warn("",e);
		}catch(IllegalAccessException e) {			
			LOGGER.warn("",e);
		}
		// on laisse passer le reste
		// à gérer dans le web.xml		

	}


}
