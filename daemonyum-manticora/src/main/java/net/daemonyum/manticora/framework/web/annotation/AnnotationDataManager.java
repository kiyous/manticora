package net.daemonyum.manticora.framework.web.annotation;

import java.lang.reflect.Field;
import java.util.Arrays;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import net.daemonyum.ajatar.annotation.data.Clean;
import net.daemonyum.ajatar.annotation.data.FillAnyFromRequest;
import net.daemonyum.ajatar.annotation.data.FillFromApplication;
import net.daemonyum.ajatar.annotation.data.FillFromRequest;
import net.daemonyum.ajatar.annotation.data.FillFromSession;
import net.daemonyum.ajatar.annotation.data.RecoverCookie;
import net.daemonyum.ajatar.annotation.data.SaveCookie;
import net.daemonyum.ajatar.annotation.data.SendToApplication;
import net.daemonyum.ajatar.annotation.data.SendToRequest;
import net.daemonyum.ajatar.annotation.data.SendToSession;
import net.daemonyum.hydra.string.TestString;
import net.daemonyum.manticora.framework.web.service.view.ServletResponse;

public class AnnotationDataManager {

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(AnnotationDataManager.class);

	protected void callAnnotationGetter(HttpServletRequest req) {    	
		callAnnotationGetter(req,this.getClass());    	
	}

	protected void callAnnotationSetter(ServletResponse view, HttpServletRequest req, HttpServletResponse resp) {
		callAnnotationSetter(view, req, resp, this.getClass());
	}

	protected void callCleaner(HttpServletRequest req) {    	
		callCleaner(req,this.getClass());    	
	}

	private void callAnnotationGetter(HttpServletRequest req, Class clazz) {
		getApplicationAttributeFromAnnotation(req, clazz.getDeclaredFields());
		getSessionAttributeFromAnnotation(req, clazz.getDeclaredFields());
		getParameterFromAnnotation(req, clazz.getDeclaredFields());
		getCookieFromAnnotation(req, clazz.getDeclaredFields());

		if(!clazz.equals(AnnotationDataManager.class)) {
			callAnnotationGetter(req, clazz.getSuperclass());
		}
	}

	private void callAnnotationSetter(ServletResponse view, HttpServletRequest req, HttpServletResponse resp, Class clazz) {
		fillViewFromAnnotation(view, clazz.getDeclaredFields());    	
		fillSessionAttributeFromAnnotation(req, clazz.getDeclaredFields());        
		fillApplicationAttributeFromAnnotation(req, clazz.getDeclaredFields());        
		fillCookieFromAnnotation(resp, clazz.getDeclaredFields()); 

		if(!clazz.equals(AnnotationDataManager.class)) {
			callAnnotationSetter(view, req, resp, clazz.getSuperclass());
		}
	}

	private void callCleaner(HttpServletRequest req, Class clazz) {
		clean(req, clazz.getDeclaredFields());
		if(!clazz.equals(AnnotationDataManager.class)) {
			callCleaner(req, clazz.getSuperclass());
		}
	}   

	private void clean(HttpServletRequest req, Field[] fields) {    	
		for (Field f:fields ) {	                	
			Clean clean = f.getAnnotation(Clean.class);	                	
			if(clean!=null) {
				String name = clean.value();
				try {						
					switch(clean.scope()) {        			
						case REQUEST:req.removeAttribute(name);break;
						case SESSION:req.getSession().removeAttribute(name);break;
						case APPLICATION:req.getSession().getServletContext().removeAttribute(name);break;
					}        											
				} catch (IllegalArgumentException e) {
					// nothing
					LOGGER.warn("",e);
				}
			}
			FillFromSession cleanFromSession = f.getAnnotation(FillFromSession.class);
			if(cleanFromSession!=null) {
				String name = cleanFromSession.value();
				try {
					switch(cleanFromSession.clean()) {
						case TRUE:req.getSession().removeAttribute(name);break;
						case FALSE:break;
					}
				} catch (IllegalArgumentException e) {
					// nothing
					LOGGER.warn("",e);
				}
			}
		}
	}

	private void fillApplicationAttributeFromAnnotation(HttpServletRequest req, Field[] fields) {    	
		for (Field f:fields) {	                	
			SendToApplication sendToApplication = f.getAnnotation(SendToApplication.class);	                	
			if(sendToApplication!=null) {
				String name = sendToApplication.value();
				if(sendToApplication.condition()) {
					f.setAccessible(true);
					try {	
						Object obj = f.get(this);
						if(obj != null) {
							req.getSession().setAttribute(name, obj);	
						}
					} catch (IllegalArgumentException e) {
						// nothing
						LOGGER.warn("",e);
					} catch (IllegalAccessException e) {
						// nothing
						LOGGER.warn("",e);
					}
				}
			}
		}
	}

	private void fillCookieFromAnnotation(HttpServletResponse res, Field[] fields) {    	
		for (Field f:fields) {	                	
			SaveCookie saveAsCookie = f.getAnnotation(SaveCookie.class);	                	
			if(saveAsCookie!=null) {
				//String name = saveAsCookie.value();
				f.setAccessible(true);
				try {
					if(f.getType().equals(Cookie.class)) {
						res.addCookie((Cookie)f.get(this));
					}else {
						throw new IllegalArgumentException("Field "+f.getName()+" is not a javax.servlet.http.Cookie object.");
					}
				} catch (IllegalArgumentException e) {
					// nothing
					LOGGER.warn("",e);
				} catch (IllegalAccessException e) {
					// nothing
					LOGGER.warn("",e);
				}
			}
		}
	}

	private void fillSessionAttributeFromAnnotation(HttpServletRequest req, Field[] fields) {    	
		for (Field f:fields) {	                	
			SendToSession sendToSession = f.getAnnotation(SendToSession.class);	                	
			if(sendToSession!=null) {
				String name = sendToSession.value();
				if(sendToSession.condition()) {
					f.setAccessible(true);
					try {
						Object obj = f.get(this);
						if(obj != null) {
							req.getSession().setAttribute(name, obj);
						}
					} catch (IllegalArgumentException e) {
						// nothing
						LOGGER.warn("",e);
					} catch (IllegalAccessException e) {
						// nothing
						LOGGER.warn("",e);
					}
				}
			}
		}        

	}

	private void fillViewFromAnnotation(ServletResponse response, Field[] fields) {    	
		for (Field f : fields) {	                	
			SendToRequest sendToRequest = f.getAnnotation(SendToRequest.class);	                	
			if(sendToRequest!=null) {
				String name = sendToRequest.value();
				if(sendToRequest.condition()) {
					f.setAccessible(true);
					try {
						Object obj = f.get(this);
						if(obj != null) {
							response.setAttribute(name, obj);
						}
					} catch (IllegalArgumentException e) {
						// nothing
						LOGGER.warn("",e);
					} catch (IllegalAccessException e) {
						// nothing
						LOGGER.warn("",e);
					}
				}
			}
		}	  
	}    

	private void getApplicationAttributeFromAnnotation(HttpServletRequest req, Field[] fields) {    	
		for (Field f:fields) {
			FillFromApplication fillFromApplication = f.getAnnotation(FillFromApplication.class);
			if(fillFromApplication!=null) {
				String name = fillFromApplication.value();
				f.setAccessible(true);
				try {
					f.set(this, req.getSession().getServletContext().getAttribute(name));
				} catch (IllegalArgumentException e) {
					// nothing
					LOGGER.warn("",e);
				} catch (IllegalAccessException e) {
					// nothing
					LOGGER.warn("",e);
				}
			}
		}
	}

	private Cookie getCookie(HttpServletRequest req, String name) {
		Cookie cookie = null;
		Cookie[]cookies = req.getCookies();
		for(Cookie c : Arrays.asList(cookies)) {
			if(c.getName().equals(name)) {
				cookie = c;
			}
		}
		return cookie;
	}

	private void getCookieFromAnnotation(HttpServletRequest req, Field[] fields) {    	
		for (Field f:fields) {
			RecoverCookie recoverCookie = f.getAnnotation(RecoverCookie.class);
			if(recoverCookie!=null) {
				String name = recoverCookie.value();
				f.setAccessible(true);
				try {
					if(f.getType().equals(Cookie.class)) {        				
						f.set(this, getCookie(req,name));        				
					}else {
						throw new IllegalArgumentException("Field "+f.getName()+" is not a javax.servlet.http.Cookie object.");
					}
				} catch (IllegalArgumentException e) {
					// nothing
					LOGGER.warn("",e);
				} catch (IllegalAccessException e) {
					// nothing
					LOGGER.warn("",e);
				}
			}
		}
	}

	private void getParameterFromAnnotation(HttpServletRequest req, Field[] fields) {    	
		// uniq
		for (Field f : fields ) {	                	
			FillFromRequest fillFromRequest = f.getAnnotation(FillFromRequest.class);	                	
			if(fillFromRequest!=null) {
				String name = fillFromRequest.value();
				f.setAccessible(true);
				try {
					if(TestString.isNotEmpty(req.getAttribute(name))){ // attribute prend le pas sur parameter
						f.set(this, req.getAttribute(name));
					}else if(TestString.isNotEmpty(req.getParameter(name))) { // si non vide
						f.set(this, req.getParameter(name));
					}
				} catch (IllegalArgumentException e) {
					// nothing
					LOGGER.warn("",e);
				} catch (IllegalAccessException e) {
					// nothing
					LOGGER.warn("",e);
				}
			}        	
		}
		// list
		for (Field f : fields ) {	                	
			FillAnyFromRequest fillAnyFromRequest = f.getAnnotation(FillAnyFromRequest.class);	                	
			if(fillAnyFromRequest!=null) {
				String name = fillAnyFromRequest.value();
				f.setAccessible(true);
				try {		
					f.set(this, req.getParameterValues(name));								
				} catch (IllegalArgumentException e) {
					// nothing
					LOGGER.warn("",e);
				} catch (IllegalAccessException e) {
					// nothing
					LOGGER.warn("",e);
				}
			}        	
		}
	}

	private void getSessionAttributeFromAnnotation(HttpServletRequest req, Field[] fields) {    	
		for (Field f:fields) {
			FillFromSession fillFromSession = f.getAnnotation(FillFromSession.class);
			if(fillFromSession!=null) {
				String name = fillFromSession.value();
				f.setAccessible(true);
				try {
					f.set(this, req.getSession().getAttribute(name));
				} catch (IllegalArgumentException e) {
					// nothing
					LOGGER.warn("",e);
				} catch (IllegalAccessException e) {
					// nothing
					LOGGER.warn("",e);
				}
			}
		}
	}

}
