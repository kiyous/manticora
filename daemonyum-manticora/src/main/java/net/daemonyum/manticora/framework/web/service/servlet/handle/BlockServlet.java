package net.daemonyum.manticora.framework.web.service.servlet.handle;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.daemonyum.manticora.framework.web.exception.ForbiddenException;
import net.daemonyum.manticora.framework.web.exception.NotFoundException;

import org.apache.log4j.Logger;

/**
 * Interdit l'acces à une ressource à partir de son URL
 * @author kiyous
 *
 */
@SuppressWarnings("serial")
public class BlockServlet extends HttpServlet{
	
	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(BlockServlet.class);

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {		
		LOGGER.trace("["+this.getClass().getSimpleName()+"] service");
		LOGGER.trace("["+this.getClass().getSimpleName()+"] " + request.getRequestURL());
		throw new NotFoundException("");
	}


}
