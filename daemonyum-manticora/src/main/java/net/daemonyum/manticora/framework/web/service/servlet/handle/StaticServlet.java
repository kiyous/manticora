package net.daemonyum.manticora.framework.web.service.servlet.handle;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

@SuppressWarnings("serial")
public class StaticServlet extends HttpServlet {
	
	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(StaticServlet.class);
	
	private static final int BUFSIZE = 4096;
    private String filePath;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		ServletContext context = getServletContext();
    	String url = request.getRequestURL().toString();
		String path = url.substring(url.lastIndexOf("/static/"));
		
		path = path.substring("/static/".length());
		path = URLDecoder.decode(path, "UTF-8");
		
		String ext = path.substring(path.lastIndexOf(".")+1);
		
		String directory = (String)request.getSession().getServletContext().getAttribute(ext.toUpperCase());
		
		try {
			filePath = getServletContext().getRealPath("") + File.separator  + (!directory.trim().equals("")?(directory + File.separator):"") + path;	
			
			File file = new File(filePath);
	        int length   = 0;
	        ServletOutputStream outStream = response.getOutputStream();
	        String mimetype = context.getMimeType(filePath);
	        
	        // sets response content type
	        if (mimetype == null) {
	            mimetype = "application/octet-stream";
	        }
	        response.setContentType(mimetype);
	        response.setContentLength((int)file.length());
	        String fileName = (new File(filePath)).getName();
	        
	        // sets HTTP header
	        // response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
	        
	        byte[] byteBuffer = new byte[BUFSIZE];
	        DataInputStream in = new DataInputStream(new FileInputStream(file));
	        
	        // reads the file's bytes and writes them to the response stream
	        while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
	            outStream.write(byteBuffer,0,length);
	        }
	        
	        in.close();
	        outStream.close();
		}catch(Exception e) {
			
		}
	}
}