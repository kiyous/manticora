package net.daemonyum.manticora.framework.web.service.servlet;

import net.daemonyum.manticora.framework.web.service.SendDataMethod;
import net.daemonyum.manticora.framework.web.service.view.ServletView;

public abstract class GetServlet extends AbstractManticoraServlet {

	protected GetServlet() {
		super(SendDataMethod.GET);
	}

	@Override
	public abstract void destroy();

	public abstract ServletView doGet();
}
