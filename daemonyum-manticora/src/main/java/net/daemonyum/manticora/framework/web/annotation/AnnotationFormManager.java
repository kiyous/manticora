package net.daemonyum.manticora.framework.web.annotation;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import net.daemonyum.ajatar.annotation.form.Alphabetic;
import net.daemonyum.ajatar.annotation.form.Alphanumeric;
import net.daemonyum.ajatar.annotation.form.MaxLenght;
import net.daemonyum.ajatar.annotation.form.MinLenght;
import net.daemonyum.ajatar.annotation.form.Numeric;
import net.daemonyum.ajatar.annotation.form.RegExp;
import net.daemonyum.ajatar.annotation.form.Required;
import net.daemonyum.ajatar.annotation.form.Specific;
import net.daemonyum.hydra.string.TestString;
import net.daemonyum.manticora.framework.web.exception.WebException;

public class AnnotationFormManager {

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(AnnotationFormManager.class);

	protected boolean validate(List<String> errors) throws WebException {
		boolean validate = true;
		for(Field f: getClass().getDeclaredFields()) {
			validate &= validate(f,errors);
		}
		return validate;
	}

	private boolean checkAlphabetic(Field f) {
		boolean check = false;
		if(f.isAnnotationPresent(Alphabetic.class)) {
			//			Alphabetic alphabetic = f.getAnnotation(Alphabetic.class);
			f.setAccessible(true);
			try {								
				Object object = f.get(this);    			 
				if(object == null) {
					check = true;
				}else {
					check = TestString.isAlphabetic(object);
				}
			} catch (IllegalArgumentException e) {
				// nothing
				LOGGER.warn("",e);
			} catch (IllegalAccessException e) {
				// nothing
				LOGGER.warn("",e);
			}	        	
		}else {
			check = true;
		}        	
		return check;
	}

	private boolean checkAlphanumeric(Field f) {
		boolean check = false;
		if(f.isAnnotationPresent(Alphanumeric.class)) {
			Alphanumeric alphanumeric = f.getAnnotation(Alphanumeric.class);
			f.setAccessible(true);
			try {								
				Object object = f.get(this);    			 
				if(object == null) {
					check = true;
				}else {
					check = TestString.isAlphanumeric(object);
				}
			} catch (IllegalArgumentException e) {
				// nothing
				LOGGER.warn("",e);
			} catch (IllegalAccessException e) {
				// nothing
				LOGGER.warn("",e);
			}        	
		} else {
			check = true;
		}       	
		return check;
	}

	private boolean checkMaxLength(Field f) {
		boolean check = false;
		if(f.isAnnotationPresent(MaxLenght.class)) {
			MaxLenght maxlength = f.getAnnotation(MaxLenght.class);
			f.setAccessible(true);
			try {								
				Object object = f.get(this);    			 
				if(object == null) {
					check = true;
				}else {    				
					check = TestString.maxLength(maxlength.value(), object);      			 	
				}
			} catch (IllegalArgumentException e) {
				// nothing
				LOGGER.warn("",e);
			} catch (IllegalAccessException e) {
				// nothing
				LOGGER.warn("",e);
			}        	
		}else {
			check = true;
		}        	
		return check;
	}

	private boolean checkMinLength(Field f) {
		boolean check = false;
		if(f.isAnnotationPresent(MinLenght.class)) {
			MinLenght minlength = f.getAnnotation(MinLenght.class);
			f.setAccessible(true);
			try {								
				Object object = f.get(this);    			 
				if(object == null) {
					check = true;
				}else {    				
					check = TestString.minLength(minlength.value(), object);      			 	
				}
			} catch (IllegalArgumentException e) {
				// nothing
				LOGGER.warn("",e);
			} catch (IllegalAccessException e) {
				// nothing
				LOGGER.warn("",e);
			}        	
		}else {
			check = true;
		}        	
		return check;
	}

	private boolean checkNumeric(Field f) {
		boolean check = false;
		if(f.isAnnotationPresent(Numeric.class)) {
			// Numeric numeric = f.getAnnotation(Numeric.class);
			f.setAccessible(true);
			try {								
				Object object = f.get(this);    			 
				if(object == null) {
					check = true;
				}else {
					check = TestString.isNumeric(object);
				}
			} catch (IllegalArgumentException e) {
				// nothing
				LOGGER.warn("",e);
			} catch (IllegalAccessException e) {
				// nothing
				LOGGER.warn("",e);
			}        	
		}else {
			check = true;
		}        	
		return check;
	}

	private boolean checkRegExp(Field f) {
		boolean check = false;
		if(f.isAnnotationPresent(RegExp.class)) {
			RegExp regexp = f.getAnnotation(RegExp.class);
			String regularyExpression = regexp.value();
			f.setAccessible(true);
			try {								
				Object object = f.get(this);    			 
				if(object == null) {
					check = true;
				}else {       			 	
					check = TestString.isValid(regularyExpression, object);
				}
			} catch (IllegalArgumentException e) {
				// nothing
				LOGGER.warn("",e);
			} catch (IllegalAccessException e) {
				// nothing
				LOGGER.warn("",e);
			}        	
		}else {
			check = true;
		}        	
		return check;
	}

	private boolean checkRequired(Field f) {
		boolean check = false;
		if(f.isAnnotationPresent(Required.class)) {
			//			Required required = f.getAnnotation(Required.class);
			f.setAccessible(true);
			try {								
				Object object = f.get(this);
				check = TestString.isNotEmpty(object);
			} catch (IllegalArgumentException e) {
				// nothing
				LOGGER.warn("",e);
			} catch (IllegalAccessException e) {
				// nothing
				LOGGER.warn("",e);
			}	        	
		}else {
			check = true;
		}
		return check;
	}

	private boolean checkSpecific(Field f) {
		boolean check = false;
		if(f.isAnnotationPresent(Specific.class)) {
			//Specific specific = f.getAnnotation(Specific.class);			
			f.setAccessible(true);
			try {								
				Object object = f.get(this);    			 
				if(object == null) {
					check = true;
				}else {
					Method[] methods = getClass().getDeclaredMethods();
					for(Method m : methods) {
						if(m.isAnnotationPresent(Specific.class)) {
							Specific spe = m.getAnnotation(Specific.class);
							String target = spe.value();
							if(target.equals(f.getName())) {
								m.setAccessible(true);
								Object returnStatement = m.invoke(this, new Object[] {});
								check = (Boolean)returnStatement;
								break;
							}
						}
					}
				}
			} catch (IllegalArgumentException e) {
				// nothing
				LOGGER.warn("",e);
			} catch (IllegalAccessException e) {
				// nothing
				LOGGER.warn("",e);
			} catch (InvocationTargetException e) {
				// nothing
				LOGGER.warn("",e);
			}	        	
		} else {
			check = true;
		}       	
		return check;
	}

	private String errorKey(Field f, Class klass) {
		return "error." + getClass().getSimpleName().toLowerCase() + "."
				+ klass.getSimpleName().toLowerCase() + "." + f.getName().toLowerCase();
	}

	private String errorMessage(Field f, Class klass) throws WebException {
		String errorMessage = null;
		ResourceBundle resource = null;
		String filename = "error-message";
		try {
			resource = ResourceBundle.getBundle(filename);
		}catch(MissingResourceException e) {

		}
		if(resource == null) {
			throw new WebException("Missing properties file : " + filename);
		}
		String key = errorKey(f, klass);
		errorMessage = resource.getString(key);

		if(errorMessage == null) {
			errorMessage = "";
			LOGGER.warn("Missing key : " + key);
		}
		return errorMessage;
	}

	private boolean validate(Field f,List<String> errors) throws WebException {

		boolean required = checkRequired(f);
		boolean alphanum = true;
		boolean numeric = true;
		boolean alpha = true;
		boolean maxlength = true;
		boolean minlength = true;
		boolean regexp = true;
		boolean specific = true;

		if(required) {
			alphanum = checkAlphanumeric(f);
			numeric = checkNumeric(f);
			alpha = checkAlphabetic(f);
			maxlength = checkMaxLength(f);
			minlength = checkMinLength(f);
			regexp = checkRegExp(f);
			specific = checkSpecific(f);
		}		

		if(!required)errors.add(errorMessage(f, Required.class));
		if(!alphanum)errors.add(errorMessage(f, Alphanumeric.class));
		if(!numeric)errors.add(errorMessage(f, Numeric.class));
		if(!alpha)errors.add(errorMessage(f, Alphabetic.class));
		if(!maxlength)errors.add(errorMessage(f, MaxLenght.class));
		if(!minlength)errors.add(errorMessage(f, MinLenght.class));
		if(!regexp)errors.add(errorMessage(f, RegExp.class));
		if(!specific)errors.add(errorMessage(f, Specific.class));		

		return required && alphanum && numeric && alpha && maxlength && minlength && regexp && specific;
	}



}
