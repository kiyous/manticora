package net.daemonyum.manticora.framework.web.service.servlet;

import javax.servlet.http.HttpSession;

import net.daemonyum.manticora.framework.web.service.SendDataMethod;

/**
 * 
 * @author Kiyous
 *
 */
public abstract class DeleteServlet extends AbstractManticoraServlet {

	protected DeleteServlet() {
		super(SendDataMethod.DELETE);		
	}

	@Override
	public abstract void destroy();

	public abstract void doDelete(HttpSession session);
}
