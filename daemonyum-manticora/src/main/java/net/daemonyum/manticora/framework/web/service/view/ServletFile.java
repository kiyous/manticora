package net.daemonyum.manticora.framework.web.service.view;

/**
 * 
 * @author Kiyous
 *
 */
public interface ServletFile {
	
	String getTemplate();
	
/**
 * 
 * @return un path vers une jsp ou null.
 */
	String getContent();
	String getLinkBetweenTemplateAndContent();

}
