package net.daemonyum.manticora.framework.web.service.view;


/**
 * Cet objet représente l'url de la page à afficher suite à l'execution d'une action.
 * 
 * Le navigateur sera redirigé (via une redirection cliente HTTP 302) vers cette url 
 * 
 * 
 * @author Kiyous
 *
 */
public class ServletRedirectUrl extends ServletResponse {
	
    private final String redirectLocation;
    
    public ServletRedirectUrl(String redirectLocation) {
       this.redirectLocation = redirectLocation;      
    }
    
    public String getRedirectLocation() {
        return redirectLocation;
    }
}
