package net.daemonyum.manticora.framework.web.bean;

import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import lombok.Getter;
import net.daemonyum.manticora.framework.web.annotation.AnnotationFormManager;
import net.daemonyum.manticora.framework.web.exception.WebException;
import net.daemonyum.manticora.framework.web.service.servlet.ManticoraServlet;

/**
 * 
 * @author Kiyous
 *
 */
@Getter
public abstract class AbstractManticoraBean extends AnnotationFormManager implements ManticoraBean {

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(AbstractManticoraBean.class);

	private List<String> confirmMessages;

	private List<String> errors;
	private Boolean valid;

	public AbstractManticoraBean() {
		errors = new LinkedList<String>();
		confirmMessages = new LinkedList<String>();
	}

	public void addError(String error) {
		if(error!= null && error.length() > 0) {
			errors.add(error);
		}
	}

	public void addMessage(String message) {
		if(message != null && message.length() > 0) {
			confirmMessages.add(message);
		}
	}

	public void doValidation() throws WebException {
		LOGGER.trace("["+this.getClass().getSimpleName()+"] doValidation");
		if(valid == null) {
			valid = validate(errors);
		}
	}

	@Override
	public final void fill(ManticoraServlet d) throws WebException {
		LOGGER.trace("["+this.getClass().getSimpleName()+"] fill");
		for(Field f : getClass().getDeclaredFields()) {		

			f.setAccessible(true);
			String fieldName = f.getName();
			Field servletField = null;
			try {
				servletField = d.getClass().getDeclaredField(fieldName);
			} catch (SecurityException e) {
				throw new WebException("",e);
			} catch (NoSuchFieldException e) {
				// nothing
				LOGGER.warn(e.getMessage());
			}
			if(servletField != null) {
				servletField.setAccessible(true);				
				try {						
					f.set(this, servletField.get(d));
				} catch (IllegalArgumentException e) {
					throw new WebException("",e);
				} catch ( IllegalAccessException e) {
					throw new WebException("",e);
				}
			}

		}
	}

	@Override
	public boolean isValid() {
		return valid;
	}
}
