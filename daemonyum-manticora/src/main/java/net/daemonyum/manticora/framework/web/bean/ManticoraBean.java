package net.daemonyum.manticora.framework.web.bean;

import net.daemonyum.manticora.framework.web.exception.WebException;
import net.daemonyum.manticora.framework.web.service.servlet.ManticoraServlet;


/**  
 * Permet de manipuler et de vérifier la validité des données envoyées depuis un formulaire
 * @author Kiyous
 *
 */
public interface ManticoraBean {
	
		
	boolean isValid() throws WebException;
	
	void fill(ManticoraServlet d) throws WebException;

}
