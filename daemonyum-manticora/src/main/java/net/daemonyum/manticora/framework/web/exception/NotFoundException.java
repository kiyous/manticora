package net.daemonyum.manticora.framework.web.exception;

public class NotFoundException  extends RuntimeException {

	private static final long serialVersionUID = -1321979074601499945L;

	public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

}
