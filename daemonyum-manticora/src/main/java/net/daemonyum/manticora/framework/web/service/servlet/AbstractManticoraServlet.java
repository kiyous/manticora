package net.daemonyum.manticora.framework.web.service.servlet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import net.daemonyum.manticora.framework.web.annotation.AnnotationDataManager;
import net.daemonyum.manticora.framework.web.bean.ManticoraBean;
import net.daemonyum.manticora.framework.web.exception.ForbiddenException;
import net.daemonyum.manticora.framework.web.service.ExecuteMethod;
import net.daemonyum.manticora.framework.web.service.SendDataMethod;
import net.daemonyum.manticora.framework.web.service.view.ServletExit;
import net.daemonyum.manticora.framework.web.service.view.ServletRedirectUrl;
import net.daemonyum.manticora.framework.web.service.view.ServletResponse;
import net.daemonyum.manticora.framework.web.service.view.ServletView;

/**
 * 
 * @author Kiyous
 *
 */
abstract class AbstractManticoraServlet extends AnnotationDataManager implements ManticoraServlet {

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(AbstractManticoraServlet.class);

	private ServletContext context;    
	private Cookie[] cookies;
	private HttpServletRequest request;
	private HttpServletResponse response;
	private SendDataMethod sendType;
	private HttpSession session;

	protected AbstractManticoraServlet(SendDataMethod type) {
		this.sendType = type;
	} 

	public abstract ManticoraBean preExecute() throws ForbiddenException;

	@Override
	public final void serve(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		LOGGER.trace("["+this.getClass().getSimpleName()+"] serve");
		request = req;
		response = resp;
		cookies = req.getCookies();
		session = req.getSession();
		context = session.getServletContext();

		ExecuteMethod executeType = null;
		ServletResponse view = null;
		ManticoraBean bean;        

		callAnnotationGetter(req);        

		bean = this.preExecute();

		// execute la servlet
		String httpMethod = req.getMethod();

		switch (this.sendType) {
			case PUT:
				if (!httpMethod.equals("PUT")) {
					resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Bad Method. This url only supports PUT");
				} else {
					PutServlet putServlet = (PutServlet) this;
					//putServlet.doPut(session);
					//Interdit
					LOGGER.warn("Should use RestServlet for put action");
					resp.sendError(HttpServletResponse.SC_FORBIDDEN);
				}
				break;        	
			case DELETE:
				if (!httpMethod.equals("DELETE")) {
					resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Bad Method. This url only supports DELETE");
				} else {
					DeleteServlet deleteServlet = (DeleteServlet) this;
					//deleteServlet.doDelete(session);
					//Interdit
					LOGGER.warn("Should use RestServlet for delete action");
					resp.sendError(HttpServletResponse.SC_FORBIDDEN);
				}
				break;
			case GET:
				if (!httpMethod.equals("GET")) {
					resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Bad Method. This url only supports GET");
				} else {
					GetServlet getServlet = (GetServlet) this;
					view = getServlet.doGet();
					executeType = ExecuteMethod.DISPATCH;	               
				}
				break;
			case POST:
				if (!httpMethod.equals("POST")) {
					resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Bad Method. This url only supports POST");
				} else {
					PostServlet postServlet = (PostServlet) this;
					view = postServlet.doPost(bean);	
					executeType = ExecuteMethod.REDIRECT; 
				}
				break;
			case SERVICE:
				ServiceServlet serviceServlet = (ServiceServlet) this;
				view = serviceServlet.doGetOrPost(bean);
				executeType = ExecuteMethod.REDIRECT;
				break;        
			case AJAX: 	        
				AjaxServlet ajaxServlet = (AjaxServlet) this;
				view = ajaxServlet.doAjax(bean);
				executeType = ExecuteMethod.DISPATCH;
				break;
			case REST:
				RestServlet restServlet = (RestServlet) this;
				//				if (httpMethod.equals("GET")) {
				//					restServlet.doGet();
				//				}else if (httpMethod.equals("POST")) {
				//					restServlet.doPost(bean);
				//				}else if (httpMethod.equals("PUT")) {
				//					restServlet.doPut(bean);
				//				}else if (httpMethod.equals("DELETE")) {
				//					restServlet.doDelete(bean);
				//				}else {
				//					resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Not supported method.");
				//				}
				executeType = ExecuteMethod.EXIT;
				break;
			default:
				break;
		}

		if(view != null) {
			callAnnotationSetter(view, req, resp);
			callCleaner(req);

			switch(executeType) {
				case DISPATCH:
					this.dispatch(req, resp, (ServletView)view);
					break;
				case REDIRECT:
					this.redirect(req, resp, (ServletRedirectUrl)view);        		
					break;
				case EXIT:
					this.exit(req, resp, null);        		
					break;
			}
		}


	}

	@Deprecated
	protected final ServletContext getContext() {
		return context;
	}

	@Deprecated
	protected final Cookie[] getCookies() {
		return cookies;
	}

	@Deprecated
	protected final HttpServletRequest getRequest() {
		return request;
	}

	@Deprecated
	protected final HttpServletResponse getResponse() {
		return response;
	}

	@Deprecated
	protected final HttpSession getSession() {
		return session;
	}

	private final void dispatch(HttpServletRequest req, HttpServletResponse resp, ServletView view) throws ServletException, IOException {
		String pathView = view.getPath();
		RequestDispatcher dispatcher = req.getRequestDispatcher(pathView);
		for (Map.Entry<String, Object> attribute : view.getAttributes().entrySet()) {
			req.setAttribute(attribute.getKey(), attribute.getValue());
		}
		dispatcher.forward(req, resp);        
	}

	private final void redirect(HttpServletRequest req, HttpServletResponse resp, ServletRedirectUrl redirectUrl) throws IOException {
		String params = "";
		if(!redirectUrl.getAttributes().isEmpty()) {
			params += "?";
		}
		int i=0;
		for (Map.Entry<String, Object> attribute : redirectUrl.getAttributes().entrySet()) {
			i++;
			params += attribute.getKey()+"="+attribute.getValue();
			req.setAttribute(attribute.getKey(), attribute.getValue());
			if(i < redirectUrl.getAttributes().size()) {
				params += "&";
			}
		}
		resp.sendRedirect(redirectUrl.getRedirectLocation()+params); 
	}
	
	private final void exit(HttpServletRequest req, HttpServletResponse resp, ServletExit exit) throws IOException {
		return;
	}


}
