package net.daemonyum.manticora.framework.web.service.servlet;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * 
 * @author Kiyous
 *
 */
public abstract class StreamServlet implements ManticoraServlet {

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(StreamServlet.class);

	@Override
	public abstract void destroy();

	@Override
	public void serve(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		LOGGER.trace("in stream");

		req.getRequestURL().toString();

		Enumeration<String> e = req.getParameterNames();
		while(e.hasMoreElements()) {
			String param = e.nextElement();

			URL url = new URL(param);

			URLConnection urlConn = null;
			ServletOutputStream stream = null;
			BufferedInputStream buf = null;
			try {

				stream = resp.getOutputStream();

				urlConn = url.openConnection();
				resp.setContentLength(urlConn.getContentLength());
				buf = new BufferedInputStream(urlConn.getInputStream());
				int readBytes = 0;

				//read from the file; write to the ServletOutputStream
				while ((readBytes = buf.read()) != -1) {
					stream.write(readBytes);
				}
			}catch (IOException ioe) {
				LOGGER.warn("",ioe);
			}finally {
				if (stream != null)
					stream.close();
				if (buf != null)
					buf.close();
			}

		}

	}

}
