package net.daemonyum.manticora.framework.web.service.servlet;

import net.daemonyum.manticora.framework.web.bean.ManticoraBean;
import net.daemonyum.manticora.framework.web.service.SendDataMethod;
import net.daemonyum.manticora.framework.web.service.view.ServletView;

/**
 * 
 * @author Kiyous
 *
 */
public abstract class AjaxServlet extends AbstractManticoraServlet {

	protected AjaxServlet() {
		super(SendDataMethod.AJAX);
	}

	@Override
	public abstract void destroy();

	public abstract ServletView doAjax(ManticoraBean bean);
}
