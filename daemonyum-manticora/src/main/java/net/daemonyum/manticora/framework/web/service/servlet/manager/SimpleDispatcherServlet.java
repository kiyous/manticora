package net.daemonyum.manticora.framework.web.service.servlet.manager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SimpleDispatcherServlet extends DispatcherServlet {

	private static final long serialVersionUID = 7492722258430781961L;

	@Override
	public final void monitoring(HttpServletRequest request, HttpServletResponse response) {
		// nothing		
	}

}
