package net.daemonyum.manticora.framework.web.service;

/**
 * 
 * @author Kiyous
 * Type d'execution des servlets
 *
 */
public enum ExecuteMethod {
	
	DISPATCH,
	REDIRECT,
    EXIT,
	;

}
