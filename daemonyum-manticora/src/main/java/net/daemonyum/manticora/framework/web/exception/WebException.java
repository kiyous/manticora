package net.daemonyum.manticora.framework.web.exception;

/**
 * 
 * 
 * @author Kiyous
 *
 */
@SuppressWarnings("serial")
public class WebException extends RuntimeException {

    public WebException(String message) {
        super(message);
    }

    public WebException(String message, Throwable cause) {
        super(message, cause);
    }
}
