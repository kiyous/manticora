package net.daemonyum.manticora.framework.web.exception;

public class ForbiddenException extends RuntimeException {

	private static final long serialVersionUID = -1321979074601499945L;

	public ForbiddenException(String message) {
        super(message);
    }

    public ForbiddenException(String message, Throwable cause) {
        super(message, cause);
    }

}
