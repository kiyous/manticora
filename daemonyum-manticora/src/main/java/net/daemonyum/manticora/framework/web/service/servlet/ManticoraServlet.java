package net.daemonyum.manticora.framework.web.service.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Représente une Servlet.
 * 
 * Les utilisateurs du framework ne devrait généralement pas implémenter directement
 * cette interface, mais plutôt surcharger une des 6 servlets DaemonyumAjaxServlet,
 * DaemonyumDeleteServlet, DaemonyumGetServlet, DaemonyumPostServlet, DaemonyumPutServlet, DaemonyumServiceServlet.
 * 
 * Cette interface pourra être implémentée directement dans les cas où le traitement
 * nécessite une prise en main complète des objets HttpServletRequest et HttpServletResponse.
 * 
 * Exemple : dans le cas où la servlet est chargée de générer autre chose qu'une 
 * page web (une image, un document binaire, etc).* 
 * 
 * @author Kiyous
 *
 */
public interface ManticoraServlet {

	/**
	 * cette méthode est appelée lorsque la servlet saisse d'être déployée et qu'elle 
	 * ne sera plus utilisée.
	 * La servlet peut ainsi libérer les ressources utilisée.
	 */
	void destroy();

	void serve(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException;
}
