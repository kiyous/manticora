package net.daemonyum.manticora.framework.web.service;

/**
 * 
 * @author Kiyous
 *
 */
public enum SendDataMethod {
	
	PUT, DELETE, GET, POST, SERVICE, AJAX, REST;

}
