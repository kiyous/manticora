package net.daemonyum.manticora.framework.web.service.servlet.manager;

import java.util.Map;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import net.daemonyum.manticora.framework.web.service.servlet.ManticoraServlet;

@Builder
@Getter
@Setter
public class ContextAttribute {

	private Map<String, String> parameters;

	private Class<? extends ManticoraServlet> servlet;

}
