package net.daemonyum.manticora.framework.web.service.view;


/**
 * 
 * @author Kiyous
 *
 */
public class ServletView extends ServletResponse {

	private final String content;
	private final String link;
	private final String path;

	public ServletView(ServletFile daemonyumFile) {
		this.path = daemonyumFile.getTemplate();
		this.content = daemonyumFile.getContent();
		this.link = daemonyumFile.getLinkBetweenTemplateAndContent();
		if(content != null) {
			setAttribute(daemonyumFile.getLinkBetweenTemplateAndContent(), daemonyumFile.getContent());
		}
	}    

	/**
	 * Permet exceptionnellement de changer la vue à mapper.
	 * @param newView nom de la vue de remplacement
	 */
	public void changeContent(String newView) {
		setAttribute(link, newView);
	}

	public String getPath() {
		return path;
	}
}
