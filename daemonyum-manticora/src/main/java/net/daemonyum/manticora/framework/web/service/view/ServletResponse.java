package net.daemonyum.manticora.framework.web.service.view;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Kiyous
 *
 */
public class ServletResponse {
	
	private final HashMap<String, Object> attributes;
	
	public ServletResponse() {
		this.attributes = new HashMap<String, Object>();
	}
	
	/**
     * Permet de passer les données au template chargé de les afficher 
     * @param name nom de l'attribut
     * @param value valeur
     */
    public void setAttribute(String name, Object value) {
        this.attributes.put(name, value);
    }
    
    public Map<String, Object> getAttributes() {
        return Collections.unmodifiableMap(this.attributes);
    }

}
