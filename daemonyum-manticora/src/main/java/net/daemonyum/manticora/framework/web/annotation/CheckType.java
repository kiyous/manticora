package net.daemonyum.manticora.framework.web.annotation;

public enum CheckType {
	
	ALPHA,
	ALPHANUM,
	NUM,
	MAX,
	MIN,
	REGEX,
	REQUIRED,
	SPECIFIC,
	;

}